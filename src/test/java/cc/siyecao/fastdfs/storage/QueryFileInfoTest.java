package cc.siyecao.fastdfs.storage;


import cc.siyecao.fastdfs.FdfsTest;
import cc.siyecao.fastdfs.model.FileInfo;
import cc.siyecao.fastdfs.model.StoreFile;
import cc.siyecao.fastdfs.model.TextFile;
import cc.siyecao.fastdfs.service.IStorageService;
import cc.siyecao.fastdfs.utils.TestUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertNotNull;

/**
 * 文件查询命令
 *
 * @author lyt
 */
public class QueryFileInfoTest extends FdfsTest {
    /**
     * 日志
     */
    protected static Logger logger = LoggerFactory.getLogger( TruncateTest.class );

    @Autowired
    private IStorageService storageService;

    @Test
    public void queryFileInfoTest() {
        TextFile textFile = TestUtils.getTextFile();
        StoreFile textPath = storageService.uploadFile( textFile.getInputStream(), textFile.getFileSize(), textFile.getFileExtName() );
        assertNotNull( textPath );
        logger.debug( "上传文件路径{}", textPath );
        FileInfo fileInfo = storageService.queryFileInfo( textPath.getGroupName(), textPath.getFileName() );
        logger.debug( "queryFileInfo文件结果{}", fileInfo );
        logger.debug( "删除文件路径{}", textPath );
        int result = storageService.deleteFile( textPath.getGroupName(), textPath.getFileName() );
        logger.debug( "删除文件结果{}", result );
    }
}
