package cc.siyecao.fastdfs.command.storage;


import cc.siyecao.fastdfs.extception.FastDfsException;
import cc.siyecao.fastdfs.model.RecvPackageInfo;
import cc.siyecao.fastdfs.model.StoreFile;
import cc.siyecao.fastdfs.protocol.ProtocolConstants;
import cc.siyecao.fastdfs.uploader.Uploader;
import cc.siyecao.fastdfs.util.BytesUtil;
import cc.siyecao.fastdfs.util.ProtocolUtil;
import cc.siyecao.fastdfs.util.StringUtils;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Arrays;

/**
 * 文件上传命令
 *
 * @author lyt
 */
public class UploadFileCommand extends FdfsStorageCommand<StoreFile> {
    private byte cmd;

    /**
     * 文件上传命令
     *
     * @param storeIndex
     * @param fileExtName
     * @param fileSize
     * @param isAppenderFile
     */
    public UploadFileCommand(byte storeIndex, String groupName, String fileExtName, long fileSize, Uploader uploader, boolean isAppenderFile) {
        this.groupName = groupName;
        this.storeIndex = storeIndex;
        this.fileSize = fileSize;
        this.fileExtName = fileExtName;
        this.uploader = uploader;
        if (isAppenderFile) {
            cmd = STORAGE_PROTO_CMD_UPLOAD_APPENDER_FILE;
        } else {
            cmd = STORAGE_PROTO_CMD_UPLOAD_FILE;
        }
    }


    @Override
    protected void send(OutputStream out, Charset charset) throws Exception {
        byte[] header;
        byte[] extNameBs;
        byte[] sizeBytes;
        byte[] hexLenBytes;
        int offset;
        long bodyLen;
        extNameBs = new byte[ProtocolConstants.FDFS_FILE_EXT_NAME_MAX_LEN];
        Arrays.fill( extNameBs, (byte) 0 );
        if (StringUtils.isNotEmpty( fileExtName )) {
            byte[] bs = fileExtName.getBytes( charset );
            int extNameLen = bs.length;
            if (extNameLen > ProtocolConstants.FDFS_FILE_EXT_NAME_MAX_LEN) {
                extNameLen = ProtocolConstants.FDFS_FILE_EXT_NAME_MAX_LEN;
            }
            System.arraycopy( bs, 0, extNameBs, 0, extNameLen );
        }

        sizeBytes = new byte[1 + 1 * ProtocolConstants.FDFS_PROTO_PKG_LEN_SIZE];
        bodyLen = sizeBytes.length + ProtocolConstants.FDFS_FILE_EXT_NAME_MAX_LEN + fileSize;

        sizeBytes[0] = storeIndex;
        offset = 1;

        hexLenBytes = BytesUtil.long2buff( fileSize );
        System.arraycopy( hexLenBytes, 0, sizeBytes, offset, hexLenBytes.length );

        header = ProtocolUtil.packHeader( cmd, bodyLen, (byte) 0 );
        byte[] wholePkg = new byte[(int) (header.length + bodyLen - fileSize)];
        System.arraycopy( header, 0, wholePkg, 0, header.length );
        System.arraycopy( sizeBytes, 0, wholePkg, header.length, sizeBytes.length );
        offset = header.length + sizeBytes.length;

        System.arraycopy( extNameBs, 0, wholePkg, offset, extNameBs.length );
        offset += extNameBs.length;
        out.write( wholePkg );
        uploader.upload( out );
    }

    @Override
    protected StoreFile receive(InputStream in, Charset charset) throws Exception {
        RecvPackageInfo pkgInfo = ProtocolUtil.recvPackage( in, STORAGE_PROTO_CMD_RESP, -1 );
        this.errno = pkgInfo.errno;
        if (pkgInfo.errno != 0) {
            return null;
        }
        if (pkgInfo.body.length <= ProtocolConstants.FDFS_GROUP_NAME_MAX_LEN) {
            throw new FastDfsException( "body length: " + pkgInfo.body.length + " <= " + ProtocolConstants.FDFS_GROUP_NAME_MAX_LEN );
        }
        String newGroupName = new String( pkgInfo.body, 0, ProtocolConstants.FDFS_GROUP_NAME_MAX_LEN ).trim();
        String remoteFileName = new String( pkgInfo.body, ProtocolConstants.FDFS_GROUP_NAME_MAX_LEN, pkgInfo.body.length - ProtocolConstants.FDFS_GROUP_NAME_MAX_LEN );
        StoreFile storeFile = new StoreFile( newGroupName, remoteFileName );
        return storeFile;
    }
}
