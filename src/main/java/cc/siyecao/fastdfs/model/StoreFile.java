package cc.siyecao.fastdfs.model;

import cc.siyecao.fastdfs.config.FastDfsConfig;
import cc.siyecao.fastdfs.extception.FastDfsException;
import cc.siyecao.fastdfs.util.FastDfsUtil;
import cc.siyecao.fastdfs.util.StringUtils;

import java.nio.charset.Charset;

/**
 * 存储文件的路径信息
 *
 * @author lyt
 */
public class StoreFile {

    private String groupName;

    private String fileName;

    /**
     * 解析路径
     */
    public static final String SPLIT_GROUP_NAME_AND_FILENAME_SEPERATOR = "/";

    /**
     * group
     */
    public static final String SPLIT_GROUP_NAME = "group";

    /**
     * 存储文件路径
     *
     * @param groupName
     * @param fileName
     */
    public StoreFile(String groupName, String fileName) {
        super();
        this.groupName = groupName;
        this.fileName = fileName;
    }

    /**
     * @return the group
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName the group to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return the path
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the path to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * 获取文件全路径
     *
     * @return
     */
    public String getFullPath() {
        return this.groupName.concat( SPLIT_GROUP_NAME_AND_FILENAME_SEPERATOR ).concat( this.fileName );
    }

    /**
     * 获取文件全路径
     *
     * @return
     */
    public String getWebUrl() {
        String webUrl = FastDfsConfig.getProperty( "fdfs.webUrl", "" );
        String fileFullName = this.groupName.concat( SPLIT_GROUP_NAME_AND_FILENAME_SEPERATOR ).concat( this.fileName );
        boolean antiStealToken = Boolean.valueOf( FastDfsConfig.getProperty( "fdfs.antiStealToken" ) );
        if (antiStealToken) {
            String charset = FastDfsConfig.getProperty( "fdfs.charset", "UTF-8" );
            String secretKey = FastDfsConfig.getProperty( "fdfs.secretKey" );
            if (StringUtils.isEmpty( secretKey )) {
                throw new FastDfsException( "fdfs.secretKey is empty!" );
            }
            int ts = (int) (System.currentTimeMillis() / 1000);
            String token = FastDfsUtil.getToken( this.fileName, ts, secretKey, Charset.forName( charset ) );
            fileFullName += "?token=" + token + "&ts=" + ts;
        }
        if (webUrl.endsWith( "/" )) {
            webUrl = webUrl + fileFullName;
        } else {
            webUrl = webUrl + "/" + fileFullName;
        }
        return webUrl;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "StoreFile [groupName=" + groupName + ", fileName=" + fileName + "]";
    }

    /**
     * 从Url当中解析存储路径对象
     *
     * @param filePath 有效的路径样式为(group/path) 或者
     *                 (http://ip/group/path),路径地址必须包含group
     * @return
     */
    public static StoreFile parseUrl(String filePath) {
        String group = getGroupName( filePath );
        // 获取group起始位置
        int pathStartPos = filePath.indexOf( group ) + group.length() + 1;
        String path = filePath.substring( pathStartPos );
        return new StoreFile( group, path );
    }

    /**
     * 获取Group名称
     *
     * @param filePath
     * @return
     */
    private static String getGroupName(String filePath) {
        //先分隔开路径
        String[] paths = filePath.split( SPLIT_GROUP_NAME_AND_FILENAME_SEPERATOR );
        if (paths.length == 1) {
            throw new FastDfsException( "解析文件路径错误,有效的路径样式为(group/path) 而当前解析路径为".concat( filePath ) );
        }
        for (String item : paths) {
            if (item.indexOf( SPLIT_GROUP_NAME ) != -1) {
                return item;
            }
        }
        throw new FastDfsException( "解析文件路径错误,被解析路径url没有group,当前解析路径为".concat( filePath ) );
    }

}
