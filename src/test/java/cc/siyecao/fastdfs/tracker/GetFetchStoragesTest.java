package cc.siyecao.fastdfs.tracker;

import cc.siyecao.fastdfs.FdfsTest;
import cc.siyecao.fastdfs.model.StorageInfo;
import cc.siyecao.fastdfs.model.StoreFile;
import cc.siyecao.fastdfs.model.TextFile;
import cc.siyecao.fastdfs.service.IStorageService;
import cc.siyecao.fastdfs.service.ITrackerService;
import cc.siyecao.fastdfs.utils.TestUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertNotNull;

/**
 * 获取源服务器
 *
 * @author lyt
 */
public class GetFetchStoragesTest extends FdfsTest {
    /**
     * 日志
     */
    protected static Logger logger = LoggerFactory.getLogger( GetFetchStoragesTest.class );

    @Autowired
    private ITrackerService trackerService;

    @Autowired
    private IStorageService storageService;

    @Test
    public void test() {
        logger.debug( "##上传文件..##..uploadFile(InputStream inputStream, long fileSize, String fileExtName) start" );
        TextFile textFile = TestUtils.getTextFile();
        StoreFile textPath = storageService.uploadFile( textFile.getInputStream(), textFile.getFileSize(), textFile.getFileExtName() );
        assertNotNull( textPath );
        logger.debug( "上传文件路径{}", textPath );
        logger.debug( "getFetchStorages开始{}", textPath );
        List<StorageInfo> storageInfos = trackerService.getFetchStorages( textPath.getGroupName(), textPath.getFileName() );
        logger.debug( "getFetchStorages结果{}", storageInfos );
        logger.debug( "删除文件路径{}", textPath );
        int result = storageService.deleteFile( textPath.getGroupName(), textPath.getFileName() );
        logger.debug( "删除文件结果{}", result );
        logger.debug( "##上传文件..##..uploadFile(InputStream inputStream, long fileSize, String fileExtName) end" );
    }
}
