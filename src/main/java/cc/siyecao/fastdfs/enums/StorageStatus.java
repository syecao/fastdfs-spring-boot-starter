package cc.siyecao.fastdfs.enums;

import java.util.Arrays;

public enum StorageStatus {
    INIT( (byte) 0 ), WAIT_SYNC( (byte) 1 ), SYNCING( (byte) 2 ), IP_CHANGED( (byte) 3 ), DELETED( (byte) 4 ), OFFLINE( (byte) 5 ), ONLINE( (byte) 6 ), ACTIVE( (byte) 7 ), NONE( (byte) 99 );
    private byte value;

    public byte getValue() {
        return value;
    }

    StorageStatus(byte value) {
        this.value = value;
    }

    public static String getName(byte value) {
        StorageStatus statusEnum = Arrays.stream( StorageStatus.values() ).filter( storageStatus -> storageStatus.value == value ).findFirst().orElse( null );
        if (statusEnum == null) {
            return "UNKOWN";
        } else {
            return statusEnum.name();
        }
    }
}
