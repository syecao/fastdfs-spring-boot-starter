package cc.siyecao.fastdfs.util;

import cc.siyecao.fastdfs.protocol.ProtocolConstants;

public class FileNameUtil {
    public static String getFileExtName(String fileName) {
        String fileExtName = "";
        int nPos = fileName.lastIndexOf( '.' );
        if (nPos > 0 && fileName.length() - nPos <= ProtocolConstants.FDFS_FILE_EXT_NAME_MAX_LEN + 1) {
            fileExtName = fileName.substring( nPos + 1 );
        }
        return fileExtName;
    }
}
