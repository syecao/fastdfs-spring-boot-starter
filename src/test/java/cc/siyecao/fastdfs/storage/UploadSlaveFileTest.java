package cc.siyecao.fastdfs.storage;

import cc.siyecao.fastdfs.FdfsTest;
import cc.siyecao.fastdfs.model.OneFile;
import cc.siyecao.fastdfs.model.StoreFile;
import cc.siyecao.fastdfs.model.TextFile;
import cc.siyecao.fastdfs.service.IStorageService;
import cc.siyecao.fastdfs.utils.TestUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertNotNull;

/**
 * 从文件上传命令
 * <p>
 * <pre>
 * 使用背景
 * 使用FastDFS存储一个图片的多个分辨率的备份时，希望只记录源图的FID，
 * 并能将其它分辨率的图片与源图关联。可以使用从文件方法
 * 名词注解:
 *   主从文件是指文件ID有关联的文件，一个主文件可以对应多个从文件
 *   主文件ID = 主文件名 + 主文件扩展名
 *   从文件ID = 主文件名 + 从文件后缀名 + 从文件扩展名
 * 以缩略图场景为例：主文件为原始图片，从文件为该图片的一张或多张缩略图
 * 流程说明：
 *  1.先上传主文件（即：原文件），得到主文件FID
 *  2.然后上传从文件（即：缩略图），指定主文件FID和从文件后缀名，上传后得到从文件FID。
 *
 * 注意:
 *   FastDFS中的主从文件只是在文件ID上有联系。FastDFS server端没有记录主从文件对应关系，
 *   因此删除主文件，FastDFS不会自动删除从文件。删除主文件后，从文件的级联删除，需要由应用端来实现。
 *
 * </pre>
 *
 * @author lyt
 */
public class UploadSlaveFileTest extends FdfsTest {
    /**
     * 日志
     */
    protected static Logger logger = LoggerFactory.getLogger( UploadSlaveFileTest.class );
    @Autowired
    private IStorageService storageService;

    @Test
    public void fileTest() {
        OneFile oneFile = TestUtils.getOneFile( "/files/test.docx" );
        StoreFile storeFile = storageService.uploadFile( oneFile.getFile(), oneFile.getFileExtName() );
        assertNotNull( storeFile );
        logger.debug( "上传主文件路径{}", storeFile );
        OneFile slaveFile = TestUtils.getOneFile( "/files/test1.docx" );
        StoreFile storeSlavePath = storageService.uploadSlaveFile( storeFile.getGroupName(), storeFile.getFileName(), "test", slaveFile.getFile(), slaveFile.getFileExtName() );
        logger.debug( "上传从文件路径{}", storeSlavePath );
        logger.debug( "删除主文件路径{}", storeFile );
        int result = storageService.deleteFile( storeFile.getGroupName(), storeFile.getFileName() );
        logger.debug( "删除主文件结果{}", result );
        logger.debug( "删除从文件路径{}", storeSlavePath );
        result = storageService.deleteFile( storeSlavePath.getGroupName(), storeSlavePath.getFileName() );
        logger.debug( "删除从文件结果{}", result );
    }

    @Test
    public void streamTest() {
        TextFile textFile = TestUtils.getTextFile();
        StoreFile textPath = storageService.uploadFile( textFile.getInputStream(), textFile.getFileSize(), textFile.getFileExtName() );
        assertNotNull( textPath );
        logger.debug( "上传主文件路径{}", textPath );
        TextFile slaveTextFile = TestUtils.getTextFile();
        StoreFile storeSlavePath = storageService.uploadSlaveFile( textPath.getGroupName(), textPath.getFileName(), "_text_test", slaveTextFile.getInputStream(), slaveTextFile.getFileSize(),
                slaveTextFile.getFileExtName() );
        logger.debug( "上传从文件路径{}", storeSlavePath );
        logger.debug( "删除主文件路径{}", textPath );
        int result = storageService.deleteFile( textPath.getGroupName(), textPath.getFileName() );
        logger.debug( "删除主文件结果{}", result );
        logger.debug( "删除从文件路径{}", storeSlavePath );
        result = storageService.deleteFile( storeSlavePath.getGroupName(), storeSlavePath.getFileName() );
        logger.debug( "删除从文件结果{}", result );
    }

    @Test
    public void buffTest() {
        TextFile textFile = TestUtils.getTextFile();
        StoreFile textPath = storageService.uploadFile( textFile.getBytes(), textFile.getFileExtName() );
        assertNotNull( textPath );
        logger.debug( "上传主文件路径{}", textPath );
        TextFile slaveTextFile = TestUtils.getTextFile();
        StoreFile storeSlavePath = storageService.uploadSlaveFile( textPath.getGroupName(), textPath.getFileName(), "_text_test", slaveTextFile.getBytes(),
                slaveTextFile.getFileExtName() );
        logger.debug( "上传从文件路径{}", storeSlavePath );
        logger.debug( "删除主文件路径{}", textPath );
        int result = storageService.deleteFile( textPath.getGroupName(), textPath.getFileName() );
        logger.debug( "删除主文件结果{}", result );
        logger.debug( "删除从文件路径{}", storeSlavePath );
        result = storageService.deleteFile( storeSlavePath.getGroupName(), storeSlavePath.getFileName() );
        logger.debug( "删除从文件结果{}", result );
    }

}
