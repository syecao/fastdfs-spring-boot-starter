package cc.siyecao.fastdfs.service.impl;

import cc.siyecao.fastdfs.command.tracker.*;
import cc.siyecao.fastdfs.model.StorageInfo;
import cc.siyecao.fastdfs.protocol.GroupProtocol;
import cc.siyecao.fastdfs.protocol.StorageProtocol;
import cc.siyecao.fastdfs.server.TrackerServer;
import cc.siyecao.fastdfs.service.ITrackerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrackerServiceImpl implements ITrackerService {

    @Autowired
    private TrackerServer trackerServer;

    @Override
    public StorageInfo getStoreStorage() {
        GetStoreStorageCommand command = new GetStoreStorageCommand();
        return trackerServer.excuteCmd( command );
    }

    @Override
    public StorageInfo getStoreStorage(String groupName) {
        GetStoreStorageCommand command = new GetStoreStorageCommand( groupName );
        return trackerServer.excuteCmd( command );
    }

    @Override
    public List<StorageInfo> getStoreStorages(String groupName) {
        GetStoreStoragesCommand command = new GetStoreStoragesCommand( groupName );
        return trackerServer.excuteCmd( command );
    }

    @Override
    public StorageInfo getFetchStorage(String groupName, String fileName) {
        GetFetchStorageCommand command = new GetFetchStorageCommand( groupName, fileName, false );
        return trackerServer.excuteCmd( command );
    }

    @Override
    public List<StorageInfo> getFetchStorages(String groupName, String fileName) {
        GetFetchStoragesCommand command = new GetFetchStoragesCommand( groupName, fileName );
        return trackerServer.excuteCmd( command );
    }

    @Override
    public StorageInfo getUpdateStorage(String groupName, String fileName) {
        GetFetchStorageCommand command = new GetFetchStorageCommand( groupName, fileName, true );
        return trackerServer.excuteCmd( command );
    }

    @Override
    public List<GroupProtocol> listGroups() {
        ListGroupsCommand command = new ListGroupsCommand();
        return trackerServer.excuteCmd( command );
    }

    @Override
    public List<StorageProtocol> listStorages(String groupName) {
        ListStoragesCommand command = new ListStoragesCommand( groupName );
        return trackerServer.excuteCmd( command );
    }

    @Override
    public List<StorageProtocol> listStorages(String groupName, String storageIpAddr) {
        ListStoragesCommand command = new ListStoragesCommand( groupName, storageIpAddr );
        return trackerServer.excuteCmd( command );
    }

    @Override
    public Integer deleteStorage(String groupName, String storageIpAddr) {
        DeleteStorageCommand command = new DeleteStorageCommand( groupName, storageIpAddr );
        return trackerServer.excuteCmd( command );
    }
}
