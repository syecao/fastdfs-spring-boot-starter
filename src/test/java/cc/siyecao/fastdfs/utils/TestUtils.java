package cc.siyecao.fastdfs.utils;

import cc.siyecao.fastdfs.model.OneFile;
import cc.siyecao.fastdfs.model.TextFile;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;

/**
 * 测试工具类
 *
 * @author lyt
 */
public class TestUtils {

    public static final Charset DEFAULT_CHARSET = Charset.forName( "UTF-8" );

    private TestUtils() {
        // hide for utils
    }

    /**
     * 将String 转换为InputStream
     *
     * @param text
     * @return
     * @throws IOException
     */
    public static InputStream getTextInputStream(String text) {
        // 将String转换为InputStream
        return new ByteArrayInputStream( text.getBytes( DEFAULT_CHARSET ) );
    }

    /**
     * 获取String长度
     *
     * @param text
     * @return
     * @throws IOException
     */
    public static long getTextLength(String text) {
        return text.getBytes( DEFAULT_CHARSET ).length;
    }

    /**
     * 获取文件InputStream
     *
     * @param path
     * @return
     * @throws FileNotFoundException
     */
    public static InputStream getFileInputStream(String path) throws FileNotFoundException {
        return new FileInputStream( getFile( path ) );
    }

    /**
     * 获取文件
     *
     * @param path
     * @return
     */
    public static File getFile(String path) {
        URL url = TestUtils.class.getResource( path );
        File file = new File( url.getFile() );
        return file;
    }

    /**
     * 获取文件
     *
     * @param text
     * @return
     */
    public static TextFile getTextFile(String text) {
        TextFile textFile = new TextFile( text );
        return textFile;
    }

    /**
     * 获取随机文件
     *
     * @return
     */
    public static TextFile getTextFile() {
        TextFile textFile = new TextFile();
        return textFile;
    }

    /**
     * 获取文件
     *
     * @param path
     * @return
     */
    public static OneFile getOneFile(String path) {
        File file = getFile( path );
        return new OneFile( file );
    }

}
