package cc.siyecao.fastdfs.storage;

import cc.siyecao.fastdfs.FdfsTest;
import cc.siyecao.fastdfs.model.Metadata;
import cc.siyecao.fastdfs.model.OneFile;
import cc.siyecao.fastdfs.model.StoreFile;
import cc.siyecao.fastdfs.service.IStorageService;
import cc.siyecao.fastdfs.utils.TestUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;

/**
 * 设置文件标签
 *
 * @author lyt
 */
public class MetadataTest extends FdfsTest {
    /**
     * 日志
     */
    protected static Logger logger = LoggerFactory.getLogger( MetadataTest.class );

    @Autowired
    private IStorageService storageService;

    @Test
    public void metadataTest() {

        logger.debug( "##生成Metadata##" );
        OneFile oneFile = TestUtils.getOneFile( "/files/test.docx" );
        Set<Metadata> setMetadata = new HashSet<>();
        setMetadata.add( new Metadata( "date", "2022-01-01" ) );
        setMetadata.add( new Metadata( "name", "测试文件" ) );
        StoreFile metaPath = storageService.uploadFile( oneFile.getFile(), oneFile.getFileExtName(), setMetadata );
        assertNotNull( metaPath );
        logger.debug( "上传文件路径{}", metaPath );
        Set<Metadata> getMetadata = storageService.getMetadata( metaPath.getGroupName(), metaPath.getFileName() );
        logger.debug( "getMetadata:{}", getMetadata );

    }
}
