package cc.siyecao.fastdfs.tracker;


import cc.siyecao.fastdfs.FdfsTest;
import cc.siyecao.fastdfs.model.StorageInfo;
import cc.siyecao.fastdfs.service.ITrackerService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 获取存储节点命令
 *
 * @author lyt
 */
public class GetStoreStoragesTest extends FdfsTest {
    @Autowired
    private ITrackerService trackerService;

    @Test
    public void test() {

    }

    /**
     * query storage servers to upload file
     *
     * @param groupName the group name to upload file to, can be empty
     * @return storage servers, return null if fail
     */
    public List<StorageInfo> getStoreStorages(String groupName) {
        return null;
    }
}
