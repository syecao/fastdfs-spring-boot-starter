# Spring Boot Starter for FastDFS

This is a spring boot starter lib for [FastDFS](https://github.com/happyfish100/fastdfs).

## 介绍

在原作者YuQing与yuqih发布的java客户端1.29-SNAPSHOT基础上进行重构，规范方法、变量名称为驼峰命名，拆分原作者代码，便于学习阅读，支持SpringBoot自动导入依赖。

当前客户端单元测试全部通过，JDK环境要求 1.8，服务端版本是FastDFS_V6.07

## 在您的maven项目pom.xml中添加依赖

```xml

<dependency>
    <groupId>cc.siyecao.fastdfs</groupId>
    <artifactId>fastdfs-spring-boot-starter</artifactId>
    <version>最新版本</version>
</dependency>
```

## 配置文件application.yml中增加如下配置

```
# ===================================================================
# 分布式文件系统FDFS配置
# ===================================================================
fdfs:
  //编码字符集
  charset: UTF-8
  //是否开启token校验，开启后通过StoreFile.getWebUrl() 返回带token的链接
  antiStealToken: true
  //token生成密钥，与服务端保持一致
  secretKey: FastDFS1234567890
  ##获取接连超时时间，单位秒
  connectTimeout: 20
  ##网络保持时间，单位秒
  networkTimeout: 60
  #nginx访问地址
  webUrl: https://192.168.0.123/
  #TrackerList参数,支持多个，如果有多个用逗号分隔
  trackerList: 192.168.0.123:22122
  pool:
    ## Whether to open the fdfsConnection pool, if not, create a new fdfsConnection every time
    enabled: false
    ## Maximum waiting time when the maximum number of connections is reached, unit: millisecond, default value is 1000
    maxWaitTime: 1000
    ## max_count_per_entry: max fdfsConnection count per host:port , 0 is not limit
    maxCountPerEntry: 60
    ## connections whose the idle time exceeds this time will be closed, unit: second, default value is 3600
    maxIdleTime: 3600

```

## 在您的项目注入IStorageService或ITrackerService

```java
@Autowired
private IStorageService storageService;

@Autowired
private ITrackerService trackerService;

```

## 测试用例

#### UploadFileTest（普通文件上传），支持File对象、InputStream,字节数组三种类型上传

```java
@Test
public void fileTest(){
        OneFile oneFile=TestUtils.getOneFile("/files/test.docx");
        StoreFile storeFile=storageService.uploadFile(oneFile.getFile(),oneFile.getFileExtName());
        assertNotNull(storeFile);
        logger.debug("上传文件路径{}",storeFile.getWebUrl());
        logger.debug("删除文件路径{}",storeFile);
        int result=storageService.deleteFile(storeFile.getGroupName(),storeFile.getFileName());
        logger.debug("删除文件结果{}",result);
        }

@Test
public void streamTest(){
        TextFile textFile=TestUtils.getTextFile();
        StoreFile textPath=storageService.uploadFile(textFile.getInputStream(),textFile.getFileSize(),textFile.getFileExtName());
        assertNotNull(textPath);
        logger.debug("上传文件路径{}",textPath);
        logger.debug("删除文件路径{}",textPath);
        int result=storageService.deleteFile(textPath.getGroupName(),textPath.getFileName());
        logger.debug("删除文件结果{}",result);
        }

@Test
public void buffTest(){
        TextFile textFile=TestUtils.getTextFile();
        StoreFile textPath=storageService.uploadFile(textFile.getBytes(),textFile.getFileExtName());
        assertNotNull(textPath);
        logger.debug("上传文件路径{}",textPath);
        logger.debug("删除文件路径{}",textPath);
        int result=storageService.deleteFile(textPath.getGroupName(),textPath.getFileName());
        logger.debug("删除文件结果{}",result);
        }
```

#### UploadAppendFileTest（支持断点续传文件上传），支持File对象、InputStream,字节数组三种类型上传

```java
 @Test
public void fileTest(){
        OneFile oneFile=TestUtils.getOneFile("/files/test.docx");
        StoreFile storeFile=storageService.uploadAppenderFile(oneFile.getFile(),oneFile.getFileExtName());
        assertNotNull(storeFile);
        logger.debug("上传文件路径{}",storeFile);
        logger.debug("删除文件路径{}",storeFile);
        int result=storageService.deleteFile(storeFile.getGroupName(),storeFile.getFileName());
        logger.debug("删除文件结果{}",result);
        }

@Test
public void streamTest(){
        TextFile textFile=TestUtils.getTextFile();
        StoreFile textPath=storageService.uploadAppenderFile(textFile.getInputStream(),textFile.getFileSize(),textFile.getFileExtName());
        assertNotNull(textPath);
        logger.debug("上传文件路径{}",textPath);
        logger.debug("删除文件路径{}",textPath);
        int result=storageService.deleteFile(textPath.getGroupName(),textPath.getFileName());
        logger.debug("删除文件结果{}",result);
        }

@Test
public void buffTest(){
        TextFile textFile=TestUtils.getTextFile();
        StoreFile textPath=storageService.uploadAppenderFile(textFile.getBytes(),textFile.getFileExtName());
        assertNotNull(textPath);
        logger.debug("上传文件路径{}",textPath);
        logger.debug("删除文件路径{}",textPath);
        int result=storageService.deleteFile(textPath.getGroupName(),textPath.getFileName());
        logger.debug("删除文件结果{}",result);
        }
```

#### UploadSlaveFileTest（从文件上传），支持File对象、InputStream,字节数组三种类型上传

```java
 @Test
public void fileTest(){
        OneFile oneFile=TestUtils.getOneFile("/files/test.docx");
        StoreFile storeFile=storageService.uploadFile(oneFile.getFile(),oneFile.getFileExtName());
        assertNotNull(storeFile);
        logger.debug("上传主文件路径{}",storeFile);
        OneFile slaveFile=TestUtils.getOneFile("/files/test1.docx");
        StoreFile storeSlavePath=storageService.uploadSlaveFile(storeFile.getGroupName(),storeFile.getFileName(),"test",slaveFile.getFile(),slaveFile.getFileExtName());
        logger.debug("上传从文件路径{}",storeSlavePath);
        logger.debug("删除主文件路径{}",storeFile);
        int result=storageService.deleteFile(storeFile.getGroupName(),storeFile.getFileName());
        logger.debug("删除主文件结果{}",result);
        logger.debug("删除从文件路径{}",storeSlavePath);
        result=storageService.deleteFile(storeSlavePath.getGroupName(),storeSlavePath.getFileName());
        logger.debug("删除从文件结果{}",result);
        }

@Test
public void streamTest(){
        TextFile textFile=TestUtils.getTextFile();
        StoreFile textPath=storageService.uploadFile(textFile.getInputStream(),textFile.getFileSize(),textFile.getFileExtName());
        assertNotNull(textPath);
        logger.debug("上传主文件路径{}",textPath);
        TextFile slaveTextFile=TestUtils.getTextFile();
        StoreFile storeSlavePath=storageService.uploadSlaveFile(textPath.getGroupName(),textPath.getFileName(),"_text_test",slaveTextFile.getInputStream(),slaveTextFile.getFileSize(),
        slaveTextFile.getFileExtName());
        logger.debug("上传从文件路径{}",storeSlavePath);
        logger.debug("删除主文件路径{}",textPath);
        int result=storageService.deleteFile(textPath.getGroupName(),textPath.getFileName());
        logger.debug("删除主文件结果{}",result);
        logger.debug("删除从文件路径{}",storeSlavePath);
        result=storageService.deleteFile(storeSlavePath.getGroupName(),storeSlavePath.getFileName());
        logger.debug("删除从文件结果{}",result);
        }

@Test
public void buffTest(){
        TextFile textFile=TestUtils.getTextFile();
        StoreFile textPath=storageService.uploadFile(textFile.getBytes(),textFile.getFileExtName());
        assertNotNull(textPath);
        logger.debug("上传主文件路径{}",textPath);
        TextFile slaveTextFile=TestUtils.getTextFile();
        StoreFile storeSlavePath=storageService.uploadSlaveFile(textPath.getGroupName(),textPath.getFileName(),"_text_test",slaveTextFile.getBytes(),
        slaveTextFile.getFileExtName());
        logger.debug("上传从文件路径{}",storeSlavePath);
        logger.debug("删除主文件路径{}",textPath);
        int result=storageService.deleteFile(textPath.getGroupName(),textPath.getFileName());
        logger.debug("删除主文件结果{}",result);
        logger.debug("删除从文件路径{}",storeSlavePath);
        result=storageService.deleteFile(storeSlavePath.getGroupName(),storeSlavePath.getFileName());
        logger.debug("删除从文件结果{}",result);
        }
```

#### AppendFileTest（续传文件上传），支持File对象、InputStream,字节数组三种类型上传

```java
 @Test
public void fileTest()throws IOException{
        TextFile oneFile=TestUtils.getTextFile();
        StoreFile storeFile=storageService.uploadAppenderFile(oneFile.getBytes(),"txt");
        assertNotNull(storeFile);
        logger.debug("上传文件路径{}",storeFile);
        TextFile appenderFile=TestUtils.getTextFile();
        int result=storageService.appendFile(storeFile.getGroupName(),storeFile.getFileName(),appenderFile.getBytes());
        logger.debug("appendFile文件结果{}",result);
        logger.debug("下载文件开始{}",storeFile);
        byte[]data=storageService.downloadFile(storeFile.getGroupName(),storeFile.getFileName());
        logger.debug("下载文件结果{}",new String(data,TestUtils.DEFAULT_CHARSET));
        logger.debug("删除文件路径{}",storeFile);
        result=storageService.deleteFile(storeFile.getGroupName(),storeFile.getFileName());
        logger.debug("删除文件结果{}",result);
        }

@Test
public void streamTest(){
        TextFile textFile=TestUtils.getTextFile();
        StoreFile textPath=storageService.uploadAppenderFile(textFile.getInputStream(),textFile.getFileSize(),textFile.getFileExtName());
        assertNotNull(textPath);
        logger.debug("上传文件路径{}",textPath);
        logger.debug("删除文件路径{}",textPath);
        int result=storageService.deleteFile(textPath.getGroupName(),textPath.getFileName());
        logger.debug("删除文件结果{}",result);
        }

@Test
public void buffTest(){
        TextFile textFile=TestUtils.getTextFile();
        StoreFile textPath=storageService.uploadAppenderFile(textFile.getBytes(),textFile.getFileExtName());
        assertNotNull(textPath);
        logger.debug("上传文件路径{}",textPath);
        logger.debug("删除文件路径{}",textPath);
        int result=storageService.deleteFile(textPath.getGroupName(),textPath.getFileName());
        logger.debug("删除文件结果{}",result);
        }
```

#### TruncateTest（重置支持断点续传的文件文件大小）

```java
@Test
public void truncateTest(){
        TextFile textFile=TestUtils.getTextFile();
        StoreFile textPath=storageService.uploadAppenderFile(textFile.getInputStream(),textFile.getFileSize(),textFile.getFileExtName());
        assertNotNull(textPath);
        logger.debug("上传文件路径{}",textPath);
        int result=storageService.truncateFile(textPath.getGroupName(),textPath.getFileName(),100);
        logger.debug("truncate文件结果{}",result);
        logger.debug("删除文件路径{}",textPath);
        result=storageService.deleteFile(textPath.getGroupName(),textPath.getFileName());
        logger.debug("删除文件结果{}",result);
        }
```

#### ModifyTest（修改支持断点续传的文件），支持File对象、InputStream,字节数组三种类型

```java
@Test
public void fileTest(){
        logger.debug("##上传文件..##..uploadAppenderFile(File file, String fileExtName) start");
        OneFile oneFile=TestUtils.getOneFile("/files/test.docx");
        StoreFile storeFile=storageService.uploadAppenderFile(oneFile.getFile(),oneFile.getFileExtName());
        assertNotNull(storeFile);
        logger.debug("上传文件路径{}",storeFile);
        OneFile modifyFile=TestUtils.getOneFile("/files/test1.docx");
        int result=storageService.modifyFile(storeFile.getGroupName(),storeFile.getFileName(),modifyFile.getFile(),0);
        logger.debug("修改文件结果{}",result);
        logger.debug("删除文件路径{}",storeFile);
        result=storageService.deleteFile(storeFile.getGroupName(),storeFile.getFileName());
        logger.debug("删除文件结果{}",result);
        logger.debug("##上传文件..##..uploadAppenderFile(File file, String fileExtName) end");
        }

@Test
public void streamTest(){
        TextFile textFile=TestUtils.getTextFile();
        StoreFile textPath=storageService.uploadAppenderFile(textFile.getInputStream(),textFile.getFileSize(),textFile.getFileExtName());
        assertNotNull(textPath);
        logger.debug("上传文件路径{}",textPath);
        TextFile modifyFile=TestUtils.getTextFile();
        int result=storageService.modifyFile(textPath.getGroupName(),textPath.getFileName(),modifyFile.getInputStream(),modifyFile.getFileSize(),0);
        logger.debug("修改文件结果{}",result);
        logger.debug("删除文件路径{}",textPath);
        result=storageService.deleteFile(textPath.getGroupName(),textPath.getFileName());
        logger.debug("删除文件结果{}",result);
        }

@Test
public void buffTest(){
        TextFile textFile=TestUtils.getTextFile();
        StoreFile textPath=storageService.uploadAppenderFile(textFile.getBytes(),textFile.getFileExtName());
        assertNotNull(textPath);
        logger.debug("上传文件路径{}",textPath);
        TextFile modifyFile=TestUtils.getTextFile();
        int result=storageService.modifyFile(textPath.getGroupName(),textPath.getFileName(),modifyFile.getBytes(),0);
        logger.debug("修改文件结果{}",result);
        logger.debug("删除文件路径{}",textPath);
        result=storageService.deleteFile(textPath.getGroupName(),textPath.getFileName());
        logger.debug("删除文件结果{}",result);
        }
```

#### DownloadTest（文件下载），支持下载为OutputStream，File,byte[]

```java
@Test
public void streamTest()throws FileNotFoundException{
        TextFile textFile=TestUtils.getTextFile();
        StoreFile textPath=storageService.uploadFile(textFile.getInputStream(),textFile.getFileSize(),textFile.getFileExtName());
        assertNotNull(textPath);
        logger.debug("上传文件路径{}",textPath);
        logger.debug("下载文件开始{}",textPath);
        File file=new File("D:/test.txt");
        FileOutputStream fos=new FileOutputStream(file,true);
        int result=storageService.downloadFile(textPath.getGroupName(),textPath.getFileName(),fos);
        logger.debug("下载文件结果{}",result);
        logger.debug("删除文件路径{}",textPath);
        result=storageService.deleteFile(textPath.getGroupName(),textPath.getFileName());
        logger.debug("删除文件结果{}",result);
        }

@Test
public void fileTest(){
        TextFile textFile=TestUtils.getTextFile();
        StoreFile textPath=storageService.uploadFile(textFile.getInputStream(),textFile.getFileSize(),textFile.getFileExtName());
        assertNotNull(textPath);
        logger.debug("上传文件路径{}",textPath);
        logger.debug("下载文件开始{}",textPath);
        File file=new File("D:/test.txt");
        int result=storageService.downloadFile(textPath.getGroupName(),textPath.getFileName(),file);
        logger.debug("下载文件结果{}",result);
        logger.debug("删除文件路径{}",textPath);
        result=storageService.deleteFile(textPath.getGroupName(),textPath.getFileName());
        logger.debug("删除文件结果{}",result);
        }

@Test
public void byteTest(){
        TextFile textFile=TestUtils.getTextFile();
        StoreFile textPath=storageService.uploadFile(textFile.getInputStream(),textFile.getFileSize(),textFile.getFileExtName());
        assertNotNull(textPath);
        logger.debug("上传文件路径{}",textPath);
        logger.debug("下载文件开始{}",textPath);
        byte[]data=storageService.downloadFile(textPath.getGroupName(),textPath.getFileName());
        logger.debug("下载文件结果{}",new String(data,TestUtils.DEFAULT_CHARSET));
        logger.debug("删除文件路径{}",textPath);
        int result=storageService.deleteFile(textPath.getGroupName(),textPath.getFileName());
        logger.debug("删除文件结果{}",result);
        }
```

#### MetadataTest（元数据设置和获取）

```java
@Test
public void metadataTest(){
        logger.debug("##生成Metadata##");
        OneFile oneFile=TestUtils.getOneFile("/files/test.docx");
        Set<Metadata> setMetadata=new HashSet<>();
        setMetadata.add(new Metadata("date","2022-01-01"));
        setMetadata.add(new Metadata("name","测试文件"));
        StoreFile metaPath=storageService.uploadFile(oneFile.getFile(),oneFile.getFileExtName(),setMetadata);
        assertNotNull(metaPath);
        logger.debug("上传文件路径{}",metaPath);
        Set<Metadata> getMetadata=storageService.getMetadata(metaPath.getGroupName(),metaPath.getFileName());
        logger.debug("getMetadata:{}",getMetadata);
        }
```

#### FileNameTest（修改支持断点续传文件为普通文件）

```java
@Test
public void fileNameTest(){
        OneFile oneFile=TestUtils.getOneFile("/files/test.docx");
        StoreFile storeFile=storageService.uploadAppenderFile(oneFile.getFile(),oneFile.getFileExtName());
        assertNotNull(storeFile);
        logger.debug("上传文件路径{}",storeFile);
        StoreFile newStoreFile=storageService.regenerateAppenderFileName(storeFile.getGroupName(),storeFile.getFileName());
        logger.debug("重命名文件结果{}",newStoreFile);
        logger.debug("删除文件路径{}",storeFile);
        int result=storageService.deleteFile(storeFile.getGroupName(),storeFile.getFileName());
        logger.debug("删除文件结果{}",result);
        }
```

#### QueryFileInfoTest（文件详细信息查询）

```java
@Test
public void queryFileInfoTest(){
        TextFile textFile=TestUtils.getTextFile();
        StoreFile textPath=storageService.uploadFile(textFile.getInputStream(),textFile.getFileSize(),textFile.getFileExtName());
        assertNotNull(textPath);
        logger.debug("上传文件路径{}",textPath);
        FileInfo fileInfo=storageService.queryFileInfo(textPath.getGroupName(),textPath.getFileName());
        logger.debug("queryFileInfo文件结果{}",fileInfo);
        logger.debug("删除文件路径{}",textPath);
        int result=storageService.deleteFile(textPath.getGroupName(),textPath.getFileName());
        logger.debug("删除文件结果{}",result);
        }
```
