package cc.siyecao.fastdfs.command.tracker;

import cc.siyecao.fastdfs.command.AbstractFdfsCommand;

public abstract class FdfsTrackerCommand<T> extends AbstractFdfsCommand<T> {
    protected String groupName;
    protected String storageIpAddr;
    protected String fileName;
}
