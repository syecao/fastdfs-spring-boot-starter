package cc.siyecao.fastdfs.tracker;

import cc.siyecao.fastdfs.FdfsTest;
import cc.siyecao.fastdfs.service.ITrackerService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 移除存储服务器命令
 * 有误操作风险，暂不实现
 *
 * @author lyt
 */
public class DeleteStorageTest extends FdfsTest {
    @Autowired
    private ITrackerService trackerService;

    @Test
    public void test() {

    }

}
