/*
 * Copyright (C) 2008 Happy Fish / YuQing
 *
 * FastDFS Java Client may be copied only under the terms of the GNU Lesser
 * General Public License (LGPL).
 * Please visit the FastDFS Home Page https://github.com/happyfish100/fastdfs for more detail.
 */

package cc.siyecao.fastdfs.extception;

/**
 * My Exception
 *
 * @author Happy Fish / YuQing
 * @version Version 1.0
 */
public class FastDfsException extends RuntimeException {
    public FastDfsException(String s, Exception e) {
    }

    public FastDfsException(String message) {
        super( message );
    }
}
