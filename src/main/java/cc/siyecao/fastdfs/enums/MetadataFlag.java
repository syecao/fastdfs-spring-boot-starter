package cc.siyecao.fastdfs.enums;

public enum MetadataFlag {
    OVERWRITE( (byte) 0 ),
    MERGE( (byte) 'M' );
    private byte value;

    MetadataFlag(byte value) {
        this.value = value;
    }

    public byte getValue() {
        return value;
    }
}
