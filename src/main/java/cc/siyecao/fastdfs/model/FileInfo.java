/**
 * Copyright (C) 2008 Happy Fish / YuQing
 * <p>
 * FastDFS Java Client may be copied only under the terms of the GNU Lesser
 * General Public License (LGPL).
 * Please visit the FastDFS Home Page https://github.com/happyfish100/fastdfs for more detail.
 */

package cc.siyecao.fastdfs.model;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Server Info
 *
 * @author Happy Fish / YuQing
 * @version Version 1.23
 */
public class FileInfo {
    public static final short FILE_TYPE_NORMAL = 1;
    public static final short FILE_TYPE_APPENDER = 2;
    public static final short FILE_TYPE_SLAVE = 4;

    protected boolean fetchFromServer;
    protected short fileType;
    protected String sourceIpAddr;
    protected long fileSize;
    protected Date createTimestamp;
    protected int crc32;

    /**
     * Constructor
     *
     * @param fetchFromServer if fetch from server flag
     * @param fileType        the file type
     * @param fileSize        the file size
     * @param createTimestamp create timestamp in seconds
     * @param crc32           the crc32 signature
     * @param sourceIpAddr    the source storage ip address
     */
    public FileInfo(boolean fetchFromServer, short fileType, long fileSize,
                    int createTimestamp, int crc32, String sourceIpAddr) {
        this.fetchFromServer = fetchFromServer;
        this.fileType = fileType;
        this.fileSize = fileSize;
        this.createTimestamp = new Date( createTimestamp * 1000L );
        this.crc32 = crc32;
        this.sourceIpAddr = sourceIpAddr;
    }

    /**
     * get the fetch_from_server flag
     *
     * @return the fetch_from_server flag
     */
    public boolean getFetchFromServer() {
        return this.fetchFromServer;
    }

    /**
     * set the fetch_from_server flag
     *
     * @param fetchFromServer the fetch from server flag
     */
    public void setFetchFromServer(boolean fetchFromServer) {
        this.fetchFromServer = fetchFromServer;
    }

    /**
     * get the file type
     *
     * @return the file type
     */
    public short getFileType() {
        return this.fileType;
    }

    /**
     * set the file type
     *
     * @param fileType the file type
     */
    public void setFileType(short fileType) {
        this.fileType = fileType;
    }

    /**
     * get the source ip address of the file uploaded to
     *
     * @return the source ip address of the file uploaded to
     */
    public String getSourceIpAddr() {
        return this.sourceIpAddr;
    }

    /**
     * set the source ip address of the file uploaded to
     *
     * @param sourceIpAddr the source ip address
     */
    public void setSourceIpAddr(String sourceIpAddr) {
        this.sourceIpAddr = sourceIpAddr;
    }

    /**
     * get the file size
     *
     * @return the file size
     */
    public long getFileSize() {
        return this.fileSize;
    }

    /**
     * set the file size
     *
     * @param fileSize the file size
     */
    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    /**
     * get the create timestamp of the file
     *
     * @return the create timestamp of the file
     */
    public Date getCreateTimestamp() {
        return this.createTimestamp;
    }

    /**
     * set the create timestamp of the file
     *
     * @param createTimestamp create timestamp in seconds
     */
    public void setCreateTimestamp(int createTimestamp) {
        this.createTimestamp = new Date( createTimestamp * 1000L );
    }

    /**
     * get the file CRC32 signature
     *
     * @return the file CRC32 signature
     */
    public long getCrc32() {
        return this.crc32;
    }

    /**
     * set the create timestamp of the file
     *
     * @param crc32 the crc32 signature
     */
    public void setCrc32(int crc32) {
        this.crc32 = crc32;
    }

    /**
     * to string
     *
     * @return string
     */
    @Override
    public String toString() {
        SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
        return "FileInfo{" +
                "fetchFromServer=" + fetchFromServer +
                ", fileType=" + fileType +
                ", sourceIpAddr='" + sourceIpAddr + '\'' +
                ", fileSize=" + fileSize +
                ", createTimestamp=" + df.format( createTimestamp ) +
                ", crc32=" + crc32 +
                '}';
    }
}
