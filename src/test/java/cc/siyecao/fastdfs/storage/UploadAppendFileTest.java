package cc.siyecao.fastdfs.storage;


import cc.siyecao.fastdfs.FdfsTest;
import cc.siyecao.fastdfs.model.OneFile;
import cc.siyecao.fastdfs.model.StoreFile;
import cc.siyecao.fastdfs.model.TextFile;
import cc.siyecao.fastdfs.service.IStorageService;
import cc.siyecao.fastdfs.utils.TestUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertNotNull;

/**
 * 文件上传命令
 *
 * @author lyt
 */
public class UploadAppendFileTest extends FdfsTest {
    /**
     * 日志
     */
    protected static Logger logger = LoggerFactory.getLogger( UploadAppendFileTest.class );
    @Autowired
    private IStorageService storageService;

    @Test
    public void fileTest() {
        OneFile oneFile = TestUtils.getOneFile( "/files/test.docx" );
        StoreFile storeFile = storageService.uploadAppenderFile( oneFile.getFile(), oneFile.getFileExtName() );
        assertNotNull( storeFile );
        logger.debug( "上传文件路径{}", storeFile );
        logger.debug( "删除文件路径{}", storeFile );
        int result = storageService.deleteFile( storeFile.getGroupName(), storeFile.getFileName() );
        logger.debug( "删除文件结果{}", result );
    }

    @Test
    public void streamTest() {
        TextFile textFile = TestUtils.getTextFile();
        StoreFile textPath = storageService.uploadAppenderFile( textFile.getInputStream(), textFile.getFileSize(), textFile.getFileExtName() );
        assertNotNull( textPath );
        logger.debug( "上传文件路径{}", textPath );
        logger.debug( "删除文件路径{}", textPath );
        int result = storageService.deleteFile( textPath.getGroupName(), textPath.getFileName() );
        logger.debug( "删除文件结果{}", result );
    }

    @Test
    public void buffTest() {
        TextFile textFile = TestUtils.getTextFile();
        StoreFile textPath = storageService.uploadAppenderFile( textFile.getBytes(), textFile.getFileExtName() );
        assertNotNull( textPath );
        logger.debug( "上传文件路径{}", textPath );
        logger.debug( "删除文件路径{}", textPath );
        int result = storageService.deleteFile( textPath.getGroupName(), textPath.getFileName() );
        logger.debug( "删除文件结果{}", result );
    }

}
