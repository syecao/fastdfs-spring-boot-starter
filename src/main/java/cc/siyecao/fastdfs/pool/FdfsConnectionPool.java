package cc.siyecao.fastdfs.pool;

import cc.siyecao.fastdfs.extception.FastDfsException;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class FdfsConnectionPool {

    private FdfsConnectionFactory fdfsConnectionFactory;

    private int maxIdleTime;

    private int maxCountPerEntry;

    private int maxWaitTime;

    private InetSocketAddress inetSocketAddress;

    /**
     * total create connection pool
     */
    private AtomicInteger totalCount = new AtomicInteger();

    /**
     * free connection count
     */
    private AtomicInteger freeCount = new AtomicInteger();

    /**
     * lock
     */
    private ReentrantLock lock = new ReentrantLock( true );

    private Condition condition = lock.newCondition();

    /**
     * free connections
     */
    private LinkedList<FdfsConnection> freeFdfsConnections = new LinkedList<>();

    public FdfsConnectionPool(FdfsConnectionFactory fdfsConnectionFactory, int maxIdleTime, int maxCountPerEntry, int maxWaitTime, InetSocketAddress inetSocketAddress) {
        this.fdfsConnectionFactory = fdfsConnectionFactory;
        this.maxIdleTime = maxIdleTime;
        this.maxCountPerEntry = maxCountPerEntry;
        this.maxWaitTime = maxWaitTime;
        this.inetSocketAddress = inetSocketAddress;
    }

    public FdfsConnection getConnection() throws FastDfsException {
        lock.lock();
        try {
            FdfsConnection fdfsConnection;
            while (true) {
                if (freeCount.get() > 0) {
                    freeCount.decrementAndGet();
                    fdfsConnection = freeFdfsConnections.poll();
                    if (!fdfsConnection.isAvaliable() || (System.currentTimeMillis() - fdfsConnection.getLastAccessTime()) > maxIdleTime) {
                        closeConnection( fdfsConnection );
                        continue;
                    }
                    if (fdfsConnection.isNeedActiveTest()) {
                        boolean isActive;
                        try {
                            isActive = fdfsConnection.activeTest();
                        } catch (IOException e) {
                            System.err.println( "send to server[" + inetSocketAddress.getAddress().getHostAddress() + ":" + inetSocketAddress.getPort() + "] active test error ,emsg:" + e.getMessage() );
                            isActive = false;
                        }
                        if (!isActive) {
                            closeConnection( fdfsConnection );
                            continue;
                        } else {
                            fdfsConnection.setNeedActiveTest( false );
                        }
                    }
                } else if (maxCountPerEntry == 0 || totalCount.get() < maxCountPerEntry) {
                    fdfsConnection = fdfsConnectionFactory.create( inetSocketAddress );
                    totalCount.incrementAndGet();
                } else {
                    try {
                        if (condition.await( maxWaitTime, TimeUnit.MILLISECONDS )) {
                            //wait single success
                            continue;
                        }
                        throw new FastDfsException( "connect to server " + inetSocketAddress.getAddress().getHostAddress() + ":" + inetSocketAddress.getPort() + " fail, wait_time > " + maxWaitTime + "ms" );
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        throw new FastDfsException( "connect to server " + inetSocketAddress.getAddress().getHostAddress() + ":" + inetSocketAddress.getPort() + " fail, emsg:" + e.getMessage() );
                    }
                }
                return fdfsConnection;
            }
        } finally {
            lock.unlock();
        }
    }

    public void releaseConnection(FdfsConnection fdfsConnection) {
        if (fdfsConnection == null) {
            return;
        }
        lock.lock();
        try {
            fdfsConnection.setLastAccessTime( System.currentTimeMillis() );
            freeFdfsConnections.add( fdfsConnection );
            freeCount.incrementAndGet();
            condition.signal();
        } finally {
            lock.unlock();
        }

    }

    public void closeConnection(FdfsConnection fdfsConnection) {
        try {
            if (fdfsConnection != null) {
                totalCount.decrementAndGet();
                fdfsConnection.closeDirectly();
            }
        } catch (IOException e) {
            System.err.println( "close socket fdfsConnection error ,emsg:" + e.getMessage() );
            e.printStackTrace();
        }
    }

    public void setActiveTestFlag() {
        if (freeCount.get() > 0) {
            lock.lock();
            try {
                for (FdfsConnection freeFdfsConnection : freeFdfsConnections) {
                    freeFdfsConnection.setNeedActiveTest( true );
                }
            } finally {
                lock.unlock();
            }
        }
    }


    @Override
    public String toString() {
        return "FdfsConnectionPool{" +
                ", totalCount=" + totalCount +
                ", freeCount=" + freeCount +
                ", freeFdfsConnections =" + freeFdfsConnections +
                '}';
    }
}
