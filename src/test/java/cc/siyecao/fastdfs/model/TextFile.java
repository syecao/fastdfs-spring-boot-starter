package cc.siyecao.fastdfs.model;

import cc.siyecao.fastdfs.util.RandomUtils;
import cc.siyecao.fastdfs.utils.TestUtils;

import java.io.InputStream;

/**
 * 测试用随机字符文件
 *
 * @author lyt
 */
public class TextFile {

    private String text;

    private InputStream inputStream;

    private long fileSize;

    private String fileExtName = "txt";

    public TextFile() {
        this.text = RandomUtils.randomStr( 40, "casffsgdfgjguktujrsvsvsbgnhgcvbxvsdfsgerdgdhd" );
        this.fileSize = TestUtils.getTextLength( text );
    }

    public TextFile(String text) {
        this.text = text;
        this.fileSize = TestUtils.getTextLength( text );
    }

    public String getText() {
        return text;
    }

    public InputStream getInputStream() {
        this.inputStream = TestUtils.getTextInputStream( text );
        return inputStream;
    }

    public long getFileSize() {
        return fileSize;
    }

    public String getFileExtName() {
        return fileExtName;
    }

    public byte[] getBytes() {
        return this.text.getBytes( TestUtils.DEFAULT_CHARSET );
    }

    public void setFileExtName(String fileExtName) {
        this.fileExtName = fileExtName;
    }
}
