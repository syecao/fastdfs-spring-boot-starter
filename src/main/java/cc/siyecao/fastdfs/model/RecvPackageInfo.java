package cc.siyecao.fastdfs.model;

/**
 * receive package info
 */
public class RecvPackageInfo {
    public byte errno;
    public byte[] body;

    public RecvPackageInfo(byte errno, byte[] body) {
        this.errno = errno;
        this.body = body;
    }
}
