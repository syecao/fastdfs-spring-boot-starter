package cc.siyecao.fastdfs.storage;

import cc.siyecao.fastdfs.FdfsTest;
import cc.siyecao.fastdfs.model.OneFile;
import cc.siyecao.fastdfs.model.StoreFile;
import cc.siyecao.fastdfs.model.TextFile;
import cc.siyecao.fastdfs.service.IStorageService;
import cc.siyecao.fastdfs.utils.TestUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertNotNull;

/**
 * 文件修改命令
 *
 * @author lyt
 */
public class ModifyTest extends FdfsTest {
    /**
     * 日志
     */
    protected static Logger logger = LoggerFactory.getLogger( ModifyTest.class );
    @Autowired
    private IStorageService storageService;

    @Test
    public void fileTest() {
        logger.debug( "##上传文件..##..uploadAppenderFile(File file, String fileExtName) start" );
        OneFile oneFile = TestUtils.getOneFile( "/files/test.docx" );
        StoreFile storeFile = storageService.uploadAppenderFile( oneFile.getFile(), oneFile.getFileExtName() );
        assertNotNull( storeFile );
        logger.debug( "上传文件路径{}", storeFile );
        OneFile modifyFile = TestUtils.getOneFile( "/files/test1.docx" );
        int result = storageService.modifyFile( storeFile.getGroupName(), storeFile.getFileName(), modifyFile.getFile(), 0 );
        logger.debug( "修改文件结果{}", result );
        logger.debug( "删除文件路径{}", storeFile );
        result = storageService.deleteFile( storeFile.getGroupName(), storeFile.getFileName() );
        logger.debug( "删除文件结果{}", result );
        logger.debug( "##上传文件..##..uploadAppenderFile(File file, String fileExtName) end" );
    }

    @Test
    public void streamTest() {
        TextFile textFile = TestUtils.getTextFile();
        StoreFile textPath = storageService.uploadAppenderFile( textFile.getInputStream(), textFile.getFileSize(), textFile.getFileExtName() );
        assertNotNull( textPath );
        logger.debug( "上传文件路径{}", textPath );
        TextFile modifyFile = TestUtils.getTextFile();
        int result = storageService.modifyFile( textPath.getGroupName(), textPath.getFileName(), modifyFile.getInputStream(), modifyFile.getFileSize(), 0 );
        logger.debug( "修改文件结果{}", result );
        logger.debug( "删除文件路径{}", textPath );
        result = storageService.deleteFile( textPath.getGroupName(), textPath.getFileName() );
        logger.debug( "删除文件结果{}", result );
    }

    @Test
    public void buffTest() {
        TextFile textFile = TestUtils.getTextFile();
        StoreFile textPath = storageService.uploadAppenderFile( textFile.getBytes(), textFile.getFileExtName() );
        assertNotNull( textPath );
        logger.debug( "上传文件路径{}", textPath );
        TextFile modifyFile = TestUtils.getTextFile();
        int result = storageService.modifyFile( textPath.getGroupName(), textPath.getFileName(), modifyFile.getBytes(), 0 );
        logger.debug( "修改文件结果{}", result );
        logger.debug( "删除文件路径{}", textPath );
        result = storageService.deleteFile( textPath.getGroupName(), textPath.getFileName() );
        logger.debug( "删除文件结果{}", result );
    }

}
