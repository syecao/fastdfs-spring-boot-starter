package cc.siyecao.fastdfs.storage;

import cc.siyecao.fastdfs.FdfsTest;
import cc.siyecao.fastdfs.model.StoreFile;
import cc.siyecao.fastdfs.model.TextFile;
import cc.siyecao.fastdfs.service.IStorageService;
import cc.siyecao.fastdfs.utils.TestUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import static org.junit.Assert.assertNotNull;

/**
 * 文件下载命令
 *
 * @author lyt
 */
public class DownloadTest extends FdfsTest {
    /**
     * 日志
     */
    protected static Logger logger = LoggerFactory.getLogger( FileNameTest.class );

    @Autowired
    private IStorageService storageService;

    @Test
    public void streamTest() throws FileNotFoundException {
        TextFile textFile = TestUtils.getTextFile();
        StoreFile textPath = storageService.uploadFile( textFile.getInputStream(), textFile.getFileSize(), textFile.getFileExtName() );
        assertNotNull( textPath );
        logger.debug( "上传文件路径{}", textPath );
        logger.debug( "下载文件开始{}", textPath );
        File file = new File( "D:/test.txt" );
        FileOutputStream fos = new FileOutputStream( file, true );
        int result = storageService.downloadFile( textPath.getGroupName(), textPath.getFileName(), fos );
        logger.debug( "下载文件结果{}", result );
        logger.debug( "删除文件路径{}", textPath );
        result = storageService.deleteFile( textPath.getGroupName(), textPath.getFileName() );
        logger.debug( "删除文件结果{}", result );
    }

    @Test
    public void fileTest() {
        TextFile textFile = TestUtils.getTextFile();
        StoreFile textPath = storageService.uploadFile( textFile.getInputStream(), textFile.getFileSize(), textFile.getFileExtName() );
        assertNotNull( textPath );
        logger.debug( "上传文件路径{}", textPath );
        logger.debug( "下载文件开始{}", textPath );
        File file = new File( "D:/test.txt" );
        int result = storageService.downloadFile( textPath.getGroupName(), textPath.getFileName(), file );
        logger.debug( "下载文件结果{}", result );
        logger.debug( "删除文件路径{}", textPath );
        result = storageService.deleteFile( textPath.getGroupName(), textPath.getFileName() );
        logger.debug( "删除文件结果{}", result );
    }

    @Test
    public void byteTest() {
        TextFile textFile = TestUtils.getTextFile();
        StoreFile textPath = storageService.uploadFile( textFile.getInputStream(), textFile.getFileSize(), textFile.getFileExtName() );
        assertNotNull( textPath );
        logger.debug( "上传文件路径{}", textPath );
        logger.debug( "下载文件开始{}", textPath );
        byte[] data = storageService.downloadFile( textPath.getGroupName(), textPath.getFileName() );
        logger.debug( "下载文件结果{}", new String( data, TestUtils.DEFAULT_CHARSET ) );
        logger.debug( "删除文件路径{}", textPath );
        int result = storageService.deleteFile( textPath.getGroupName(), textPath.getFileName() );
        logger.debug( "删除文件结果{}", result );
    }

}
