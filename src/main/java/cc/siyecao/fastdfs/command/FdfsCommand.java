package cc.siyecao.fastdfs.command;


import cc.siyecao.fastdfs.pool.FdfsConnection;

/**
 * Fdfs交易命令抽象
 *
 * @author lyt
 */
public interface FdfsCommand<T> extends Command {
    /**
     * 执行交易
     */
    T execute(FdfsConnection conn);

}
