package cc.siyecao.fastdfs.config;

import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@Configuration
public class FastDfsConfig implements EnvironmentAware {
    private static Environment environment;

    /**
     * 获取属性
     *
     * @param key
     * @return
     */
    public static String getProperty(String key) {
        String value = environment.getProperty( key );
        return value;
    }

    /**
     * 获取属性
     *
     * @param key          属性key
     * @param defaultValue 属性value
     * @return
     */
    public static String getProperty(String key, String defaultValue) {
        String value = environment.getProperty( key );
        value = value == null ? defaultValue : value;
        return value;
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}
