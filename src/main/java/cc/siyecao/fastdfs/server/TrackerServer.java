/**
 * Copyright (C) 2008 Happy Fish / YuQing
 * <p>
 * FastDFS Java Client may be copied only under the terms of the GNU Lesser
 * General Public License (LGPL).
 * Please visit the FastDFS Home Page https://github.com/happyfish100/fastdfs for more detail.
 */

package cc.siyecao.fastdfs.server;

import cc.siyecao.fastdfs.command.FdfsCommand;
import cc.siyecao.fastdfs.extception.FastDfsException;
import cc.siyecao.fastdfs.pool.FdfsConnection;
import cc.siyecao.fastdfs.pool.FdfsConnectionManager;
import cc.siyecao.fastdfs.pool.TrackerLocator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * Tracker Server Info
 *
 * @author Happy Fish / YuQing
 * @version Version 1.11
 */
@Component
public class TrackerServer {
    @Autowired
    private TrackerLocator trackerLocator;

    @Autowired
    private FdfsConnectionManager fdfsConnectionManager;

    public FdfsConnection getConnection() throws FastDfsException {
        InetSocketAddress inetSockAddr = trackerLocator.getTrackerAddress();
        return fdfsConnectionManager.getConnection( inetSockAddr );
    }

    public <T> T excuteCmd(FdfsCommand<T> fdfsCommand) {
        FdfsConnection fdfsConnection = null;
        try {
            fdfsConnection = getConnection();
            return fdfsCommand.execute( fdfsConnection );
        } catch (Exception exception) {
            try {
                fdfsConnection.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                fdfsConnection = null;
            }
            exception.printStackTrace();
            throw new FastDfsException( "exception occured while sending cmd", exception );
        } finally {
            if (fdfsConnection != null) {
                try {
                    fdfsConnection.release();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
