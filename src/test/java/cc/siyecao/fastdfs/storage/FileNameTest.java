package cc.siyecao.fastdfs.storage;

import cc.siyecao.fastdfs.FdfsTest;
import cc.siyecao.fastdfs.model.OneFile;
import cc.siyecao.fastdfs.model.StoreFile;
import cc.siyecao.fastdfs.service.IStorageService;
import cc.siyecao.fastdfs.utils.TestUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertNotNull;

/**
 * 文件修改命令
 *
 * @author lyt
 */
public class FileNameTest extends FdfsTest {
    /**
     * 日志
     */
    protected static Logger logger = LoggerFactory.getLogger( FileNameTest.class );

    @Autowired
    private IStorageService storageService;

    @Test
    public void fileNameTest() {
        OneFile oneFile = TestUtils.getOneFile( "/files/test.docx" );
        StoreFile storeFile = storageService.uploadAppenderFile( oneFile.getFile(), oneFile.getFileExtName() );
        assertNotNull( storeFile );
        logger.debug( "上传文件路径{}", storeFile );
        StoreFile newStoreFile = storageService.regenerateAppenderFileName( storeFile.getGroupName(), storeFile.getFileName() );
        logger.debug( "重命名文件结果{}", newStoreFile );
        logger.debug( "删除文件路径{}", storeFile );
        int result = storageService.deleteFile( storeFile.getGroupName(), storeFile.getFileName() );
        logger.debug( "删除文件结果{}", result );
    }

}
