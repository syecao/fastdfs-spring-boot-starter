package cc.siyecao.fastdfs.command.storage;

import cc.siyecao.fastdfs.model.RecvPackageInfo;
import cc.siyecao.fastdfs.protocol.ProtocolConstants;
import cc.siyecao.fastdfs.util.ProtocolUtil;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Arrays;

/**
 * 文件删除命令
 *
 * @author lyt
 */
public class DeleteFileCommand extends FdfsStorageCommand<Integer> {

    /**
     * 文件删除命令
     *
     * @param groupName 存储节点
     * @param groupName 删除文件
     */
    public DeleteFileCommand(String groupName, String remoteFileName) {
        this.groupName = groupName;
        this.fileName = remoteFileName;
    }

    @Override
    protected void send(OutputStream out, Charset charset) throws Exception {
        byte[] header;
        byte[] groupBytes;
        byte[] filenameBytes;
        byte[] bs;
        int groupLen;

        groupBytes = new byte[ProtocolConstants.FDFS_GROUP_NAME_MAX_LEN];
        bs = groupName.getBytes( charset );
        filenameBytes = fileName.getBytes( charset );

        Arrays.fill( groupBytes, (byte) 0 );
        if (bs.length <= groupBytes.length) {
            groupLen = bs.length;
        } else {
            groupLen = groupBytes.length;
        }
        System.arraycopy( bs, 0, groupBytes, 0, groupLen );

        header = ProtocolUtil.packHeader( STORAGE_PROTO_CMD_DELETE_FILE, groupBytes.length + filenameBytes.length, (byte) 0 );
        byte[] wholePkg = new byte[header.length + groupBytes.length + filenameBytes.length];
        System.arraycopy( header, 0, wholePkg, 0, header.length );
        System.arraycopy( groupBytes, 0, wholePkg, header.length, groupBytes.length );
        System.arraycopy( filenameBytes, 0, wholePkg, header.length + groupBytes.length, filenameBytes.length );
        out.write( wholePkg );
    }

    @Override
    protected Integer receive(InputStream in, Charset charset) throws Exception {
        RecvPackageInfo pkgInfo = ProtocolUtil.recvPackage( in, STORAGE_PROTO_CMD_RESP, 0 );
        if (pkgInfo.errno != 0) {
            return Integer.valueOf( this.errno );
        }
        return 0;
    }
}
