/**
 * Copyright (C) 2008 Happy Fish / YuQing
 * <p>
 * FastDFS Java Client may be copied only under the terms of the GNU Lesser
 * General Public License (LGPL).
 * Please visit the FastDFS Home Page https://github.com/happyfish100/fastdfs for more detail.
 */

package cc.siyecao.fastdfs.protocol;

import cc.siyecao.fastdfs.config.FastDfsConfig;
import cc.siyecao.fastdfs.util.BytesUtil;

import java.io.UnsupportedEncodingException;
import java.util.Date;

/**
 * C struct body decoder
 *
 * @author Happy Fish / YuQing
 * @version Version 1.17
 */
public abstract class Protocol {
    /**
     * set fields
     *
     * @param bs     byte array
     * @param offset start offset
     */
    public abstract void setFields(byte[] bs, int offset);

    protected String stringValue(byte[] bs, int offset, FieldInfo filedInfo) {
        try {
            return (new String( bs, offset + filedInfo.offset, filedInfo.size, FastDfsConfig.getProperty( "fdfs.charset", "utf-8" ) )).trim();
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    protected long longValue(byte[] bs, int offset, FieldInfo filedInfo) {
        return BytesUtil.buff2long( bs, offset + filedInfo.offset );
    }

    protected int intValue(byte[] bs, int offset, FieldInfo filedInfo) {
        return (int) BytesUtil.buff2long( bs, offset + filedInfo.offset );
    }

    protected int int32Value(byte[] bs, int offset, FieldInfo filedInfo) {
        return BytesUtil.buff2int( bs, offset + filedInfo.offset );
    }

    protected byte byteValue(byte[] bs, int offset, FieldInfo filedInfo) {
        return bs[offset + filedInfo.offset];
    }

    protected boolean booleanValue(byte[] bs, int offset, FieldInfo filedInfo) {
        return bs[offset + filedInfo.offset] != 0;
    }

    protected Date dateValue(byte[] bs, int offset, FieldInfo filedInfo) {
        return new Date( BytesUtil.buff2long( bs, offset + filedInfo.offset ) * 1000 );
    }

}
