package cc.siyecao.fastdfs.tracker;

import cc.siyecao.fastdfs.FdfsTest;
import cc.siyecao.fastdfs.protocol.StorageProtocol;
import cc.siyecao.fastdfs.service.ITrackerService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 列出组命令
 *
 * @author lyt
 */
public class ListStoragesTest extends FdfsTest {
    @Autowired
    private ITrackerService trackerService;

    @Test
    public void test() {

    }

    /**
     * query storage server stat info of the group
     *
     * @param groupName the group name of storage server
     * @return storage server stat array, return null if fail
     */

    public List<StorageProtocol> listStorages(String groupName) {
        return null;
    }
}
