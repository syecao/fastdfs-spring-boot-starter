package cc.siyecao.fastdfs.service;

import cc.siyecao.fastdfs.model.StorageInfo;
import cc.siyecao.fastdfs.protocol.GroupProtocol;
import cc.siyecao.fastdfs.protocol.StorageProtocol;

import java.util.List;

public interface ITrackerService {
    /**
     * query storage server to upload file
     *
     * @return storage server Socket object, return null if fail
     */
    StorageInfo getStoreStorage();

    /**
     * query storage server to upload file
     *
     * @param groupName the group name to upload file to, can be empty
     * @return storage server object, return null if fail
     */
    StorageInfo getStoreStorage(String groupName);

    /**
     * query storage servers to upload file
     *
     * @param groupName the group name to upload file to, can be empty
     * @return storage servers, return null if fail
     */
    List<StorageInfo> getStoreStorages(String groupName);

    /**
     * 获取读取存储节点 get the fetchStorage Client by group and filename
     *
     * @param groupName
     * @param fileName
     * @return
     */
    StorageInfo getFetchStorage(String groupName, String fileName);

    /**
     * get storage servers to download file
     *
     * @param groupName the group name of storage server
     * @param fileName  fileName on storage server
     * @return storage servers, return null if fail
     */
    List<StorageInfo> getFetchStorages(String groupName, String fileName);

    /**
     * query storage server to update file (delete file or set meta data)
     *
     * @param groupName the group name of storage server
     * @param fileName  fileName on storage server
     * @return storage server Socket object, return null if fail
     */
    StorageInfo getUpdateStorage(String groupName, String fileName);

    /**
     * list groups
     *
     * @return group stat array, return null if fail
     */
    List<GroupProtocol> listGroups();

    /**
     * query storage server stat info of the group
     *
     * @param groupName the group name of storage server
     * @return storage server stat array, return null if fail
     */
    List<StorageProtocol> listStorages(String groupName);

    /**
     * query storage server stat info of the group
     *
     * @param groupName     the group name of storage server
     * @param storageIpAddr the storage server ip address, can be null or empty
     * @return storage server stat array, return null if fail
     */
    List<StorageProtocol> listStorages(String groupName, String storageIpAddr);

    /**
     * delete a storage server from the tracker server
     *
     * @param groupName     the group name of storage server
     * @param storageIpAddr the storage server ip address
     * @return true for success, false for fail
     */
    Integer deleteStorage(String groupName, String storageIpAddr);
}
