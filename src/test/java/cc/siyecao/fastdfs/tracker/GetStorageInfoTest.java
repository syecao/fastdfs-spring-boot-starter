package cc.siyecao.fastdfs.tracker;


import cc.siyecao.fastdfs.FdfsTest;
import cc.siyecao.fastdfs.model.StorageInfo;
import cc.siyecao.fastdfs.service.ITrackerService;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 获取存储节点命令
 *
 * @author lyt
 */
public class GetStorageInfoTest extends FdfsTest {
    /**
     * 日志
     */
    protected static Logger logger = LoggerFactory.getLogger( GetStorageInfoTest.class );

    @Autowired
    private ITrackerService trackerService;

    @Test
    public void test() {
        logger.debug( "####..getStoreStorage() start" );
        StorageInfo storageInfo = trackerService.getStoreStorage();
        logger.debug( "getStoreStorage结果{}", storageInfo );
    }

}
