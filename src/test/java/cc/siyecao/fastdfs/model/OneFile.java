package cc.siyecao.fastdfs.model;

import cc.siyecao.fastdfs.util.FileNameUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * 测试用随机字符文件
 *
 * @author lyt
 */
public class OneFile {

    private File file;

    private InputStream inputStream;

    private long fileSize;

    private String fileExtName;

    public OneFile(File file) {
        this.file = file;
        this.fileSize = file.length();
        this.fileExtName = FileNameUtil.getFileExtName( file.getName() );
    }

    public InputStream getInputStream() throws FileNotFoundException {
        return new FileInputStream( this.file );
    }

    public File getFile() {
        return file;
    }

    public long getFileSize() {
        return fileSize;
    }

    public String getFileExtName() {
        return fileExtName;
    }
}
