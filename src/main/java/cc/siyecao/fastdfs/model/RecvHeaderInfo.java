package cc.siyecao.fastdfs.model;

/**
 * receive header info
 */
public class RecvHeaderInfo {
    public byte errno;
    public long bodyLen;

    public RecvHeaderInfo(byte errno, long bodyLen) {
        this.errno = errno;
        this.bodyLen = bodyLen;
    }
}
