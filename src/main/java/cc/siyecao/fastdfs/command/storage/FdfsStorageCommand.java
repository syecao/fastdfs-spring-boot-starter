package cc.siyecao.fastdfs.command.storage;

import cc.siyecao.fastdfs.command.AbstractFdfsCommand;
import cc.siyecao.fastdfs.downloader.Downloader;
import cc.siyecao.fastdfs.model.Metadata;
import cc.siyecao.fastdfs.uploader.Uploader;

import java.util.Set;

public abstract class FdfsStorageCommand<T> extends AbstractFdfsCommand<T> {
    protected String groupName;
    protected String fileName;
    protected long fileOffset;
    protected long fileSize;
    protected Uploader uploader;
    protected Downloader downloader;
    protected Set<Metadata> metaSet;
    protected byte opFlag;
    protected String prefixName;//the prefix name to generate the slave file
    protected String fileExtName;//file ext name, do not include dot(.)
    protected byte storeIndex;
}
