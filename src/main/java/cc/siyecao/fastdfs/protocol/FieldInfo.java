package cc.siyecao.fastdfs.protocol;

public class FieldInfo {
    protected String name;
    protected int offset;
    protected int size;

    public FieldInfo(String name, int offset, int size) {
        this.name = name;
        this.offset = offset;
        this.size = size;
    }
}
