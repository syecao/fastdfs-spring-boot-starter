package cc.siyecao.fastdfs.storage;

import cc.siyecao.fastdfs.FdfsTest;
import cc.siyecao.fastdfs.model.StoreFile;
import cc.siyecao.fastdfs.model.TextFile;
import cc.siyecao.fastdfs.service.IStorageService;
import cc.siyecao.fastdfs.utils.TestUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;

/**
 * 添加文件命令
 *
 * @author lyt
 */
public class AppendFileTest extends FdfsTest {
    /**
     * 日志
     */
    protected static Logger logger = LoggerFactory.getLogger( AppendFileTest.class );

    @Autowired
    private IStorageService storageService;

    @Test
    public void fileTest() throws IOException {
        TextFile oneFile = TestUtils.getTextFile();
        StoreFile storeFile = storageService.uploadAppenderFile( oneFile.getBytes(), "txt" );
        assertNotNull( storeFile );
        logger.debug( "上传文件路径{}", storeFile );
        TextFile appenderFile = TestUtils.getTextFile();
        int result = storageService.appendFile( storeFile.getGroupName(), storeFile.getFileName(), appenderFile.getBytes() );
        logger.debug( "appendFile文件结果{}", result );
        logger.debug( "下载文件开始{}", storeFile );
        byte[] data = storageService.downloadFile( storeFile.getGroupName(), storeFile.getFileName() );
        logger.debug( "下载文件结果{}", new String( data, TestUtils.DEFAULT_CHARSET ) );
        logger.debug( "删除文件路径{}", storeFile );
        result = storageService.deleteFile( storeFile.getGroupName(), storeFile.getFileName() );
        logger.debug( "删除文件结果{}", result );
    }

    @Test
    public void streamTest() {
        TextFile textFile = TestUtils.getTextFile();
        StoreFile textPath = storageService.uploadAppenderFile( textFile.getInputStream(), textFile.getFileSize(), textFile.getFileExtName() );
        assertNotNull( textPath );
        logger.debug( "上传文件路径{}", textPath );
        logger.debug( "删除文件路径{}", textPath );
        int result = storageService.deleteFile( textPath.getGroupName(), textPath.getFileName() );
        logger.debug( "删除文件结果{}", result );
    }

    @Test
    public void buffTest() {
        TextFile textFile = TestUtils.getTextFile();
        StoreFile textPath = storageService.uploadAppenderFile( textFile.getBytes(), textFile.getFileExtName() );
        assertNotNull( textPath );
        logger.debug( "上传文件路径{}", textPath );
        logger.debug( "删除文件路径{}", textPath );
        int result = storageService.deleteFile( textPath.getGroupName(), textPath.getFileName() );
        logger.debug( "删除文件结果{}", result );
    }

}
