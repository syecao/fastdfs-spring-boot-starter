package cc.siyecao.fastdfs.tracker;

import cc.siyecao.fastdfs.FdfsTest;
import cc.siyecao.fastdfs.protocol.GroupProtocol;
import cc.siyecao.fastdfs.service.ITrackerService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 列出组命令
 *
 * @author lyt
 */
public class ListGroupsTest extends FdfsTest {
    @Autowired
    private ITrackerService trackerService;

    @Test
    public void test() {

    }

    /**
     * list groups
     *
     * @return group stat array, return null if fail
     */
    public List<GroupProtocol> listGroups() {
        return null;
    }
}
