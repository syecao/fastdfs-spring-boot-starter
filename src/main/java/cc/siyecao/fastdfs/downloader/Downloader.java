/**
 * Copyright (C) 2008 Happy Fish / YuQing
 * <p>
 * FastDFS Java Client may be copied only under the terms of the GNU Lesser
 * General Public License (LGPL).
 * Please visit the FastDFS Home Page https://github.com/happyfish100/fastdfs for more detail.
 */

package cc.siyecao.fastdfs.downloader;

/**
 * Download file callback interface
 *
 * @author Happy Fish / YuQing
 * @version Version 1.4
 */
public interface Downloader {
    /**
     * recv file content callback function, may be called more than once when the file downloaded
     *
     * @param fileSize file size
     * @param buff     data buff
     * @param data     data bytes
     * @return 0 success, return none zero(errno) if fail
     */
    int download(long fileSize, byte[] buff, int data);
}
