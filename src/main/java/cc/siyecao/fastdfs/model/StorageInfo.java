/**
 * Copyright (C) 2008 Happy Fish / YuQing
 * <p>
 * FastDFS Java Client may be copied only under the terms of the GNU Lesser
 * General Public License (LGPL).
 * Please visit the FastDFS Home Page https://github.com/happyfish100/fastdfs for more detail.
 */

package cc.siyecao.fastdfs.model;

import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * Storage Server Info
 *
 * @author Happy Fish / YuQing
 * @version Version 1.11
 */
public class StorageInfo {
    private int storePathIndex = 0;
    private String ipAddr;
    private int port;

    /**
     * @return the inetSocketAddress
     */
    public InetSocketAddress getInetSocketAddress() {
        return new InetSocketAddress( ipAddr, port );
    }

    /**
     * Constructor
     *
     * @param ipAddr    the ip address of storage server
     * @param port      the port of storage server
     * @param storePath the store path index on the storage server
     */
    public StorageInfo(String ipAddr, int port, byte storePath) throws IOException {
        this.ipAddr = ipAddr;
        this.port = port;
        if (storePath < 0) {
            this.storePathIndex = 256 + storePath;
        } else {
            this.storePathIndex = storePath;
        }
    }

    /**
     * @return the store path index on the storage server
     */
    public int getStorePathIndex() {
        return this.storePathIndex;
    }

    @Override
    public String toString() {
        return "StorageInfo{" +
                "storePathIndex=" + storePathIndex +
                ", ipAddr='" + ipAddr + '\'' +
                ", port=" + port +
                '}';
    }
}
