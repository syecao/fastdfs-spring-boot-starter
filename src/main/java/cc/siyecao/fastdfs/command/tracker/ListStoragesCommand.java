package cc.siyecao.fastdfs.command.tracker;

import cc.siyecao.fastdfs.model.RecvPackageInfo;
import cc.siyecao.fastdfs.protocol.ProtocolConstants;
import cc.siyecao.fastdfs.protocol.ProtocolDecoder;
import cc.siyecao.fastdfs.protocol.StorageProtocol;
import cc.siyecao.fastdfs.util.ProtocolUtil;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

/**
 * 列出组命令
 *
 * @author lyt
 */
public class ListStoragesCommand extends FdfsTrackerCommand<List<StorageProtocol>> {

    public ListStoragesCommand(String groupName, String storageIpAddr) {
        this.groupName = groupName;
        this.storageIpAddr = storageIpAddr;
    }

    public ListStoragesCommand(String groupName) {
        this.groupName = groupName;
    }

    @Override
    protected void send(OutputStream out, Charset charset) throws Exception {
        byte[] header;
        byte[] bGroupName;
        byte[] bs;
        int len;
        bs = groupName.getBytes( charset );
        bGroupName = new byte[ProtocolConstants.FDFS_GROUP_NAME_MAX_LEN];

        if (bs.length <= ProtocolConstants.FDFS_GROUP_NAME_MAX_LEN) {
            len = bs.length;
        } else {
            len = ProtocolConstants.FDFS_GROUP_NAME_MAX_LEN;
        }
        Arrays.fill( bGroupName, (byte) 0 );
        System.arraycopy( bs, 0, bGroupName, 0, len );

        int ipAddrLen;
        byte[] bIpAddr;
        if (storageIpAddr != null && storageIpAddr.length() > 0) {
            bIpAddr = storageIpAddr.getBytes( charset );
            if (bIpAddr.length < ProtocolConstants.FDFS_IPADDR_SIZE) {
                ipAddrLen = bIpAddr.length;
            } else {
                ipAddrLen = ProtocolConstants.FDFS_IPADDR_SIZE - 1;
            }
        } else {
            bIpAddr = null;
            ipAddrLen = 0;
        }

        header = ProtocolUtil.packHeader( TRACKER_PROTO_CMD_SERVER_LIST_STORAGE, ProtocolConstants.FDFS_GROUP_NAME_MAX_LEN + ipAddrLen, (byte) 0 );
        byte[] wholePkg = new byte[header.length + bGroupName.length + ipAddrLen];
        System.arraycopy( header, 0, wholePkg, 0, header.length );
        System.arraycopy( bGroupName, 0, wholePkg, header.length, bGroupName.length );
        if (ipAddrLen > 0) {
            System.arraycopy( bIpAddr, 0, wholePkg, header.length + bGroupName.length, ipAddrLen );
        }
        out.write( wholePkg );
    }

    @Override
    protected List<StorageProtocol> receive(InputStream in, Charset charset) throws Exception {
        RecvPackageInfo pkgInfo = ProtocolUtil.recvPackage( in, TRACKER_PROTO_CMD_RESP, -1 );
        this.errno = pkgInfo.errno;
        if (pkgInfo.errno != 0) {
            return null;
        }
        ProtocolDecoder<StorageProtocol> decoder = new ProtocolDecoder<StorageProtocol>();
        StorageProtocol[] storageProtocols = decoder.decode( pkgInfo.body, StorageProtocol.class, StorageProtocol.getFieldsTotalSize() );
        return Arrays.asList( storageProtocols );
    }
}
