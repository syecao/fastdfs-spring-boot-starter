package cc.siyecao.fastdfs.uploader;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Upload file by file buff
 *
 * @author Happy Fish / YuQing
 * @version Version 1.12
 */
public class BuffUploader implements Uploader {
    private byte[] fileBuff;
    private int offset;
    private int length;

    /**
     * constructor
     *
     * @param fileBuff the file buff for uploading
     */
    public BuffUploader(byte[] fileBuff, int offset, int length) {
        super();
        this.fileBuff = fileBuff;
        this.offset = offset;
        this.length = length;
    }

    /**
     * send file content callback function, be called only once when the file uploaded
     *
     * @param out output stream for writing file content
     * @return 0 success, return none zero(errno) if fail
     */
    @Override
    public int upload(OutputStream out) throws IOException {
        out.write( this.fileBuff, this.offset, this.length );
        return 0;
    }
}
